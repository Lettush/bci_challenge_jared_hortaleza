# BCI Challenge for Script Writers

Rev: 1
Author: Nicolas Marchand
Date: 21/08/19

## Introduction

As part of the recruitment process, you are required to take a technical test.
This test will enable us to assess the following:
- python knowledge
- http/front-end knowledge
- coding logic
- coding best practices and hygiene

## Instructions

- We propose you 4 challenges in total, in 4 different folders.
- You must complete your work (see deliverables below) within 7 days of receiving this document.
- You do not have to complete all challenges, but the more the better.
- You can do the challenges in any sequence you want, though we advise to follow the numbers.
- Each folder contains 2 files: a python file with the URL of the website to mine, and a csv file with an example of what we expect as output of your work.

## Deliverables

- You will create and provide us with access to a BitBucket repository named like "BCI_Challenge_First Name_Last Name", containing all your work and organized as 1 folder per challenge.
- For each challenge, minimum 2 files are expected: a python file with your code, and a csv with the output of your code.
- Your code should be self-explanatory and commented. A readme file should be added if necessary.