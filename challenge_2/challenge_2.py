"""
Difficulty: medium
"""

# imports
from datetime import datetime
from bs4 import BeautifulSoup
import requests
import pandas as pd
from dateutil.parser import parse

def is_date(string, fuzzy=False):
    """
    Checks if a string is a date
    """
    try: 
        parse(string, fuzzy=fuzzy)
        return True

    except ValueError:
        return False

# url request and soup initialization
url_only = "https://businesspapers.parracity.nsw.gov.au/"
url = requests.get('https://businesspapers.parracity.nsw.gov.au/').text
soup = BeautifulSoup(url, 'lxml')

children = soup.find_all("td", {'class': "bpsGridDate"})

last_div = None
for last_div in children:
    pass

parent = last_div.findParent()
data = []
filters = ['\nMinutes HTML\nMinutes PDF\n', '\nAgenda HTML\nAgenda PDF\n', '\n', '\nAgenda PDF\n']


while True:
    # list per entry
    entry = []

    # checks children if before the first entry of sample csv
    if parent.findChildren()[0].text != "24 Aug 20211:30pm":
        # iterates over children
        for child in parent.findChildren():
            if child.find("span"): 
                if child == parent.findChildren()[0]:
                    span = child.find("span")
                    span.decompose()
                else:
                    span = child.find("span")
                    span.replaceWith(f'\n{span.getText()}')

            if is_date(child.text):
                date = datetime.strptime(child.text, "%d %b %Y")
                entry.append(datetime.strftime(date, "%Y-%m-%d %H:%M:%S+10:05"))
            else:
                if child.name == "a":
                    if child['href']:
                        link = child['href']
                        entry.append(f'{url_only}{link}')
                elif child.text not in filters and child.text != '' and child.name != 'span':
                    entry.append(child.text)

        data.append(entry[:-1])
        # next sibling
        parent = parent.findPreviousSibling()
    else:
        # stops loop
        break

# converts list to pandas dataframe for csv exporting
csv = pd.DataFrame(data)
csv.iloc[::-1]

# headers and conversion
header_list = ['meeting_date', 'meeting_description', 'agenda_html_link', 'agenda_pdf_link', 'minutes'] 
csv.to_csv("data.csv", index=False, header=header_list)