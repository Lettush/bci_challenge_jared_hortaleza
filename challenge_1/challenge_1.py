"""
Difficulty: easy
"""

# imports
from datetime import datetime
from bs4 import BeautifulSoup
import requests
import pandas as pd
from dateutil.parser import parse

# url request and soup initialization
url = requests.get('https://www.emservices.com.sg/tenders/').text
soup = BeautifulSoup(url, 'lxml')

def is_date(string, fuzzy=False):
    """
    Checks if a string is a date
    """
    try: 
        parse(string, fuzzy=fuzzy)
        return True

    except ValueError:
        return False

# finds last entry for the list
parent = soup.find("td", text="23/07/2021").findParent().findPreviousSibling()

# parent list
data = []

# loops until break
while True:
    # list per entry
    entry = []

    # checks children if before the first entry of sample csv
    if parent.findChildren()[0].text != "20/08/2021" and parent.findChildren()[2].text != "WEST COAST TOWN COUNCIL":
        # iterates over children
        for child in parent.findChildren():
            # checks if date
            if is_date(str(child.text)):
                date = datetime.strptime(child.text, "%d/%m/%Y")
                entry.append(datetime.strftime(date, "%Y-%m-%d %H:%M:%S"))

            # if not download, append
            else:
                if (child.text != "Download"):
                    entry.append(child.text)
        # append entry to data
        data.append(entry)
        # next sibling
        parent = parent.findPreviousSibling()
    else:
        # stops loop
        break

# converts list to pandas dataframe for csv exporting
csv = pd.DataFrame(data)

# headers and conversion
header_list = ['advert_date', 'closing_dates', 'client', 'description', 'eligibility', 'link'] 
csv.to_csv("data.csv", index=False, header=header_list)